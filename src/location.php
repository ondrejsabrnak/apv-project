<?php

function show_location($db, $id_location) {
    $stmt = $db->prepare("SELECT * FROM location WHERE id_location = :id_location");
    $stmt->bindValue(':id_location', $id_location);
    $stmt->execute();
    return $stmt->fetch();
}

function insert_lokace($db, $formData) {
   $stmt = $db->prepare("INSERT INTO location (city, street_name, street_number, zip, country) VALUES (:city, :street_name, :street_number, :zip, :country)");
   $stmt->bindValue(':city', empty($formData['city']) ? null : $formData['city']);
   $stmt->bindValue(':street_name', empty($formData['street_name']) ? null : $formData['street_name']);
   $stmt->bindValue(':street_number', empty($formData['street_number']) ? null : $formData['street_number']);
   $stmt->bindValue(':zip', empty($formData['zip']) ? null : $formData['zip']);
   $stmt->bindValue(':country', empty($formData['country']) ? null : $formData['country']);
   $stmt->execute();
   return $db->lastInsertId('location_id_location_seq');
}

function update_lokace($db, $formData, $id_lokace) {
    $stmt = $db->prepare("UPDATE location SET 
                                city = :city,
                                street_name = :street_name,
                                street_number = :street_number,
                                zip = :zip,
                                country = :country
                            WHERE id_location = :id_location");
    $stmt->bindValue(':city', empty($formData['city']) ? null : $formData['city']);
    $stmt->bindValue(':street_name', empty($formData['street_name']) ? null : $formData['street_name']);
    $stmt->bindValue(':street_number', empty($formData['street_number']) ? null : $formData['street_number']);
    $stmt->bindValue(':zip', empty($formData['zip']) ? null : $formData['zip']);
    $stmt->bindValue(':country', empty($formData['country']) ? null : $formData['country']);
    $stmt->bindValue('id_location', $id_lokace);
    $stmt->execute();
}