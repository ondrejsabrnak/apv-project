<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

include 'persons.php';
include 'contact.php';
include 'meetings.php';

// Funkce na vytvoreni zpravy
function message($type, $message) {
    return $message = [
        "type" => $type,
        "message" => $message
    ];
}

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

/* Vypis osob */
$app->get('/osoby', function (Request $request, Response $response, $args) {
    $strana = $request->getQueryParam('strana');
    $tplVars['strana'] = $strana;
    $tplVars['celkovyPocet'] = count_persons($this->db);
    $tplVars['osoby'] = show_persons_page($this->db,$strana);
    return $this->view->render($response, 'osoby.latte', $tplVars);
})->setName('osoby');

/* Vyhladavani osob */
$app->post('/osoby/vyhledat', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (!empty($formData)) {
        switch ($formData['hledatPodle']) {
            case 'first_name':
                $tplVars['osoby'] = find_by_first_name($this->db, $formData);
                break;
            case 'last_name':
                $tplVars['osoby'] = find_by_last_name($this->db, $formData);
                break;
            case 'nickname':
                $tplVars['osoby'] = find_by_nickname($this->db, $formData);
                break;
            default:
                $tplVars['osoby'] = [];
                break;
        }
        $tplVars['zobrazeno'] = true;
        return $this->view->render($response, 'osoby.latte', $tplVars);
    }
})->setName('vyhledat');

/* Formular pro insert nove osoby*/
$app->get('/osoby/nova', function (Request $request, Response $response, $args) {
    $tplVars['formData'] = [
        'first_name' => '',
        'last_name' => '',
        'nickname' => '',
        'id_location' => null,
        'gender' => '',
        'height' => '',
        'birth_day' => '',
        'city' => '',
        'street_number' => '',
        'street_name' => '',
        'country' => '',
        'zip' => ''
    ];
    $tplVars['nadpis'] = 'Nová osoba';
    return $this->view->render($response, 'novaOsoba.latte', $tplVars);
})->setName('osoba_nova');

/* Obsluha formu pro novou osoby*/
$app->post('/osoby/nova', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname']) ) {
        $tplVars['message'] = message('danger', 'Prosím vyplňte povinné údaje.');
    } else {
        try {
            insert_person($this->db, $formData);
            $tplVars['message'] = message('success', 'Osoba byla přidána.');
        } catch (PDOException $e) {
            $tplVars['message'] = message('danger', 'Error: ' . $e->getMessage());
        }
    }
    $tplVars['nadpis'] = 'Nová osoba';
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'novaOsoba.latte', $tplVars);
});

$app->get('/osoby/uprava', function (Request $request, Response $response, $args) {
    $params = $request->getQueryParams();
    if (empty($params['id_person'])) {
        exit('Chybí ID osoby');
    } else {
        $stmt = $this->db->prepare("SELECT * FROM person LEFT JOIN location on person.id_location = location.id_location WHERE person.id_person = :id_person");
        $stmt->bindValue(':id_person', $params['id_person']);
        $stmt->execute();
        $tplVars['formData'] = $stmt->fetch();
        $tplVars['nadpis'] = 'Úprava osoby';
        if (empty($tplVars['formData'])) {
            exit("osoba nenalezena");
        } else {
            return $this->view->render($response, 'upravOsoba.latte', $tplVars);
        }
    }
})->setName('osoba_uprav');

/* obsluha formu pre update osoby */
$app->post('/osoby/uprava', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = $request->getQueryParam('id_person');
    if (empty($formData['first_name']) || empty($formData['last_name']) || empty($formData['nickname']) || empty($id_person)) {
        $tplVars['message'] = message('danger', 'Prosím vyplňte povinné údaje.');
    } else {
        try {
            update_person($this->db, $formData, $id_person);
            $tplVars['message'] = message('success', 'Osoba upravena.');
        } catch (PDOexception $e) {
            $tplVars['message'] = message('danger', 'Error: ' . $e->getMessage());
        }
    }
    $tplVars['nadpis'] = 'Úprava osoby';
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'upravOsoba.latte', $tplVars);
});

$app->get('/osoby/zobraz', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    if (empty($id_person)) {
        exit('Chybí ID osoby');
    } else {
        $tplVars['location'] = show_location($this->db, getLocationID($this->db,$id_person));
        $tplVars['formData'] = show_person($this->db, $id_person);
        $tplVars['contactData'] = show_contacts($this->db, $id_person);
        if (empty($tplVars['formData'])) {
            exit("osoba nenalezena");
        } else {
            return $this->view->render($response, 'zobrazOsoba.latte', $tplVars);
        }
    }
})->setName('osoba_zobraz');

$app->get('/osoby/smazat', function (Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    $strana = $request->getQueryParam('strana');
    if (!empty($id_person)) {
        try {
            delete_person($this->db, $id_person);
            $tplVars['message'] = message('success', 'Osoba byla smazána.');
        } catch (PDOException $exception) {
            $tplVars['message'] = message('danger', 'Error ' . $exception->getMessage());
        }
        $tplVars['strana'] = $strana;
        $tplVars['celkovyPocet'] = count_persons($this->db);
        $tplVars['osoby'] = show_persons_page($this->db, $strana);
        return $this->view->render($response, 'osoby.latte', $tplVars);
    } else {
        exit("Chybi ID osoby.");
    }
})->setName('osoba_smazat');

$app->get('/osoby/zobraz/kontakt/pridej', function(Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    if (empty($id_person)) {
        exit('Chybí ID osoby');
    } else {
        $tplVars['contact_types'] = getContactTypes($this->db);
        $tplVars['formData'] = [
            'id_contact_type' => '',
            'contact' => ''
        ];
        $tplVars['id_person'] = $id_person;
        $tplVars['nadpis'] = 'Přidej kontakt';
        return $this->view->render($response, 'pridejKontakt.latte', $tplVars);
    }
})->setName('kontakt_pridej');

$app->post('/osoby/zobraz/kontakt/pridej', function(Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_person = $request->getQueryParam('id_person');
    if (empty($formData['id_contact_type']) || empty($formData['contact']) ) {
        $tplVars['message'] = message('danger', 'Prosím vyplňte povinné údaje.');
    } else {
        try {
            insert_contact($this->db,$formData, $id_person);
            $tplVars['message'] = message('success', 'Kontakt byl přidán.');
        } catch (PDOException $e) {
            $tplVars['message'] = message('danger', 'Error: ' . $e->getMessage());
        }
    }
    $tplVars['location'] = show_location($this->db, getLocationID($this->db,$id_person));
    $tplVars['contactData'] = show_contacts($this->db, $id_person);
    $tplVars['formData'] = show_person($this->db, $id_person);
    return $this->view->render($response, 'zobrazOsoba.latte', $tplVars);
});

$app->get('/osoby/zobraz/kontakt/smaz', function(Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    $id_contact = $request->getQueryParam('id_contact');
    if (empty($id_person) || empty($id_contact)) {
        exit('Chybí ID person nebo ID contact.');
    } else {
        try {
            delete_contact($this->db, $id_contact);
            $tplVars['message'] = message('success', 'Kontakt byl smazán.');
        } catch (PDOException $exception) {
            $tplVars['message'] = message('danger', 'Error: ' . $exception->getMessage());
        }
    }
    $tplVars['location'] = show_location($this->db, getLocationID($this->db,$id_person));
    $tplVars['contactData'] = show_contacts($this->db, $id_person);
    $tplVars['formData'] = show_person($this->db, $id_person);
    return $this->view->render($response, 'zobrazOsoba.latte', $tplVars);
})->setName('kontakt_smaz');

$app->get('/schuzky', function (Request $request, Response $response, $args) {
    $tplVars['meetings'] = show_meetings($this->db);
    $tplVars['nadpis'] = 'Schůzky';
    return $this->view->render($response, 'meetings.latte', $tplVars);
})->setName('meetings');

$app->get('/schuzky/zobraz', function (Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParam('id_schuzky');
    if (empty($id_meeting)) {
        exit('Chybí ID schuzky');
    } else {
        $tplVars['meeting'] = show_meeting($this->db, $id_meeting);
        $tplVars['osoby'] = show_persons_by_id_meeting($this->db, $id_meeting);
        if (empty($tplVars['meeting'])) {
            exit("schuzka nenalezena");
        } else {
            return $this->view->render($response, 'zobrazSchuzka.latte', $tplVars);
        }
    }
})->setName('meeting_zobraz');

$app->get('/schuzky/smazat', function (Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParam('id_meeting');
    if (!empty($id_meeting)) {
        try {
            delete_meeting($this->db, $id_meeting);
            $tplVars['message'] = message('success', 'Schůzka byla smazána.');
        } catch (PDOException $exception) {
            $tplVars['message'] = message('danger', 'Error ' . $exception->getMessage());
        }
        $tplVars['meetings'] = show_meetings($this->db);
        $tplVars['nadpis'] = 'Schůzky';
        return $this->view->render($response, 'meetings.latte', $tplVars);
    } else {
        exit("Chybi ID osoby.");
    }
})->setName('schuzka_smazat');

$app->get('/schuzka/osoba/smaz', function(Request $request, Response $response, $args) {
    $id_person = $request->getQueryParam('id_person');
    $id_schuzka = $request->getQueryParam('id_schuzka');
    if (empty($id_schuzka) || empty($id_person)) {
        exit('Chybí ID osoby nebo ID schůzky.');
    } else {
        try {
            delete_person_from_meeting($this->db, $id_schuzka, $id_person);
            $tplVars['message'] = message('success', 'Osoba byla odebrána ze schůzky.');
        } catch (PDOException $exception) {
            $tplVars['message'] = message('danger', 'Error: ' . $exception->getMessage());
        }
    }
    $tplVars['meeting'] = show_meeting($this->db, $id_schuzka);
    $tplVars['osoby'] = show_persons_by_id_meeting($this->db, $id_schuzka);
    return $this->view->render($response, 'zobrazSchuzka.latte', $tplVars);
})->setName('schuzka_osoba_smaz');

$app->get('/schuzka/osoba/pridej', function(Request $request, Response $response, $args) {
    $id_meeting = $request->getQueryParam('id_schuzka');
    if (empty($id_meeting)) {
        exit('Chybí ID schůzky');
    } else {
        $tplVars['osoby'] = show_persons($this->db);
        $tplVars['id_meeting'] = $id_meeting;
        $tplVars['nadpis'] = 'Přidej osobu';
        return $this->view->render($response, 'pridejOsobuDoSchuzky.latte', $tplVars);
    }
})->setName('schuzka_osoba_pridej');

$app->post('/schuzka/osoba/pridej', function(Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    $id_meeting = $request->getQueryParam('id_schuzka');
    if (empty($formData['id_person'])) {
        $tplVars['message'] = message('danger', 'Prosím vyplňte povinné údaje.');
    } else {
        try {
            insert_person_into_meeting($this->db,$id_meeting,$formData['id_person']);
            $tplVars['message'] = message('success', 'Osoba byla přidána.');
        } catch (PDOException $e) {
            $tplVars['message'] = message('danger', 'Error: ' . $e->getMessage());
        }
    }
    $tplVars['meeting'] = show_meeting($this->db, $id_meeting);
    $tplVars['osoby'] = show_persons_by_id_meeting($this->db, $id_meeting);
    return $this->view->render($response, 'zobrazSchuzka.latte', $tplVars);
});

$app->get('/schuzka/nova', function (Request $request, Response $response, $args) {
    $tplVars['nadpis'] = 'Nová schůzka';
    return $this->view->render($response, 'novaSchuzka.latte', $tplVars);
})->setName('schuzka_nova');

/* Obsluha formu pro novou osoby*/
$app->post('/schuzka/nova', function (Request $request, Response $response, $args) {
    $formData = $request->getParsedBody();
    if (empty($formData['date']) || empty($formData['time']) || empty($formData['time_zone']) ) {
        $tplVars['message'] = message('danger', 'Prosím vyplňte povinné pole.');
    } else {
        try {
            insert_meeting($this->db, $formData);
            $tplVars['message'] = message('success', 'Schůzka byla přidána.');
        } catch (PDOException $e) {
            $tplVars['message'] = message('danger', 'Error: ' . $e->getMessage());
        }
    }
    $tplVars['nadpis'] = 'Nová schůzka';
    $tplVars['formData'] = $formData;
    return $this->view->render($response, 'novaSchuzka.latte', $tplVars);
});