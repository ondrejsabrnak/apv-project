<?php

function show_contacts($db, $id_person) {
    $stmt = $db->prepare("SELECT person.id_person, contact.contact, contact_type.name, contact.id_contact FROM person LEFT JOIN contact ON person.id_person = contact.id_person LEFT JOIN contact_type ON contact.id_contact_type = contact_type.id_contact_type WHERE person.id_person = :id_person");
    $stmt->bindValue(':id_person', $id_person);
    $stmt->execute();
    return $stmt->fetchAll();
}


function insert_contact($db, $formData, $id_person) {
    $stmt = $db->prepare("INSERT INTO contact (id_person, id_contact_type, contact) VALUES (:id_person, :id_contact_type, :contact)");
    $stmt->bindValue(':id_person', $id_person);
    $stmt->bindValue(':id_contact_type', $formData['id_contact_type']);
    $stmt->bindValue(':contact', $formData['contact']);
    $stmt->execute();
}

function delete_contact($db, $id_contact) {
    $stmt = $db->prepare("DELETE FROM contact WHERE id_contact = :id_contact");
    $stmt->bindValue(':id_contact', $id_contact);
    $stmt->execute();
}

function getContactTypes($db) {
    $stmt = $db->prepare("SELECT * FROM contact_type");
    $stmt->execute();
    return $stmt->fetchAll();
}


