<?php

function show_meetings($db) {
    $stmt = $db->query("SELECT meeting_start.*, meeting.*, COALESCE(pocet_osob ,0) AS pocet_osob
                            FROM meeting_start LEFT JOIN meeting ON meeting_start.id_meeting_start = meeting.id_meeting_start
                            LEFT JOIN (SELECT id_meeting, count(id_person) AS pocet_osob 
                            FROM person_meeting 
                            GROUP by id_meeting) AS pm USING (id_meeting)
                            ORDER BY meeting_start.date, meeting_start.time, duration");
    return $stmt->fetchAll();
}

function show_meeting($db, $id_meeting) {
    $stmt = $db->prepare("SELECT meeting_start.*, meeting.*, location.*, COALESCE(pocet_osob ,0) AS pocet_osob
                            FROM meeting_start LEFT JOIN meeting ON meeting_start.id_meeting_start = meeting.id_meeting_start
                            LEFT JOIN (SELECT id_meeting, count(id_person) AS pocet_osob 
                            FROM person_meeting 
                            GROUP by id_meeting) AS pm USING (id_meeting)
                            LEFT JOIN location USING (id_location)
                            WHERE id_meeting = :id_meeting
                            ORDER BY meeting_start.date, meeting_start.time, duration");
    $stmt->bindValue(':id_meeting', $id_meeting);
    $stmt->execute();
    return $stmt->fetch();
}

function delete_meeting($db, $id_meeting) {
    $stmt = $db->prepare("DELETE FROM meeting WHERE id_meeting = :id");
    $stmt->bindValue(':id', $id_meeting);
    $stmt->execute();
}

function show_persons_by_id_meeting($db, $id_meeting) {
    $stmt = $db->prepare("SELECT * FROM person_meeting LEFT JOIN person ON person_meeting.id_person = person.id_person WHERE id_meeting = :id_meeting");
    $stmt->bindValue(':id_meeting', $id_meeting);
    $stmt->execute();
    return $stmt->fetchAll();
}

function delete_person_from_meeting($db, $id_meeting, $id_person) {
    $stmt = $db->prepare("DELETE FROM person_meeting WHERE id_meeting = :id_meeting AND id_person = :id_person");
    $stmt->bindValue('id_meeting', $id_meeting);
    $stmt->bindValue('id_person', $id_person);
    $stmt->execute();
}

function insert_person_into_meeting($db, $id_meeting, $id_person){
    $stmt = $db->prepare('INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)');
    $stmt->bindValue(':id_person', $id_person);
    $stmt->bindValue(':id_meeting', $id_meeting);
    $stmt->execute();
}

function insert_meeting_start($db, $formData) {
    $stmt = $db->prepare("INSERT INTO meeting_start (date, time, time_zone) VALUES (:date, :time, :time_zone)");
    $stmt->bindValue(':date', $formData['date']);
    $stmt->bindValue(':time', $formData['time']);
    $stmt->bindValue(':time_zone', $formData['time_zone']);
    $stmt->execute();
    return $db->lastInsertId('meeting_start_id_meeting_start_seq');
}

function insert_meeting($db, $formData) {
    // Vlozeni lokace
    $id_location = insert_lokace($db, $formData);

    // Vlozeni casu konani schuzky
    $id_meeting_start = insert_meeting_start($db, $formData);

    // Vlozeni meetingu
    $stmt = $db->prepare("INSERT INTO meeting 
        (description, duration, id_location, id_meeting_start)
        VALUES (:description, :duration, :id_location, :id_meeting_start)");
    $stmt->bindValue(":description", $formData['description']);
    $stmt->bindValue(":duration", $formData['duration']);
    $stmt->bindValue(":id_location", $id_location);
    $stmt->bindValue(":id_meeting_start", $id_meeting_start);
    $stmt->execute();
}