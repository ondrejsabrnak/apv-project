<?php

include 'location.php';

function show_persons_page($db, $strana) {
    $stmt = $db->prepare('SELECT * FROM person ORDER BY first_name LIMIT 10 OFFSET :strana');
    if (!$strana) {$strana = 0;}
    $stmt->bindParam(':strana', $strana);
    $stmt->execute();
    return $stmt->fetchAll();
}

function show_persons($db) {
    $stmt = $db->query('SELECT * FROM person ORDER BY first_name');
    return $stmt->fetchAll();
}

function count_persons($db){
    $stmt = $db->query('SELECT count(*) FROM person');
    return $stmt->fetch()['count'];
}

function show_person($db, $id_person) {
    $stmt = $db->prepare("SELECT * FROM person WHERE person.id_person = :id_person");
    $stmt->bindValue(':id_person', $id_person);
    $stmt->execute();
    return $stmt->fetch();
}

function find_by_first_name($db, $formData) {
    $stmt = $db->prepare("SELECT id_person, first_name, last_name, gender, nickname FROM person
            WHERE (lower(first_name) LIKE :hledaneSlovo)
            ORDER BY last_name, first_name");
    $stmt->bindValue(':hledaneSlovo', strtolower($formData['hledaneSlovo'] . '%'));
    $stmt->execute();
    return $stmt->fetchAll();
}

function find_by_last_name($db, $formData) {
    $stmt = $db->prepare("SELECT id_person, first_name, last_name, gender, nickname FROM person
            WHERE (lower(last_name) LIKE :hledaneSlovo)
            ORDER BY last_name, first_name");
    $stmt->bindValue(':hledaneSlovo', strtolower($formData['hledaneSlovo'] . '%'));
    $stmt->execute();
    return $stmt->fetchAll();
}

function find_by_nickname($db, $formData) {
    $stmt = $db->prepare("SELECT id_person, first_name, last_name, gender, nickname FROM person
            WHERE (lower(nickname) LIKE :hledaneSlovo)
            ORDER BY last_name, first_name");
    $stmt->bindValue(':hledaneSlovo', strtolower($formData['hledaneSlovo'] . '%'));
    $stmt->execute();
    return $stmt->fetchAll();
}

function insert_person($db, $formData) {
    if (!empty($formData['city']) || !empty($formData['street_name']) || !empty($formData['stret_number']) || !empty($formData['zip']) || !empty($formData['country'])) {
        $id_location = insert_lokace($db, $formData);
    } else {
        $id_location = null;
    }

    $stmt = $db->prepare("INSERT INTO person 
        (nickname, first_name, last_name, id_location, birth_day, height, gender)
        VALUES (:nickname, :first_name, :last_name, :id_location, :birth_day, :height, :gender)");
    $stmt->bindValue(":nickname", $formData['nickname']);
    $stmt->bindValue(":last_name", $formData['last_name']);
    $stmt->bindValue(":first_name", $formData['first_name']);
    $stmt->bindValue(":id_location", $id_location ? $id_location : null);
    $stmt->bindValue(":gender", empty($formData['gender']) ? null : $formData['gender']);
    $stmt->bindValue(":height", empty($formData['height']) ? null : $formData['height']);
    $stmt->bindValue(":birth_day", empty($formData['birth_day']) ? null : $formData['birth_day']);
    $stmt->execute();
}

function update_person($db, $formData, $id_person) {
    update_lokace($db,$formData,getLocationID($db, $id_person));
    $stmt = $db->prepare("UPDATE person SET 
                                first_name = :fn,
                                last_name = :ln,
                                nickname = :nn,
                                birth_day = :bd,
                                gender = :gn,
                                height = :hg
                            WHERE id_person = :idp");
    $stmt->bindValue(":fn", $formData['first_name']);
    $stmt->bindValue(":ln", $formData['last_name']);
    $stmt->bindValue(":nn", $formData['nickname']);
    $stmt->bindValue(":gn", empty($formData['gender']) ? null : $formData['gender']);
    $stmt->bindValue(":hg", empty($formData['height']) ? null : $formData['height']);
    $stmt->bindValue(":bd", empty($formData['birth_day']) ? null : $formData['birth_day']);
    $stmt->bindValue(":idp", $id_person);
    $stmt->execute();
}

function delete_person($db, $id_person) {
    $stmt = $db->prepare("DELETE FROM person WHERE id_person = :id");
    $stmt->bindValue(':id', $id_person);
    $stmt->execute();
}

function getLocationID($db, $id_person) {
    $stmt = $db->prepare("SELECT id_location FROM person WHERE id_person = :id_person");
    $stmt->bindValue(":id_person", $id_person);
    $stmt->execute();
    $data = $stmt->fetch();
    return $data['id_location'];
}

